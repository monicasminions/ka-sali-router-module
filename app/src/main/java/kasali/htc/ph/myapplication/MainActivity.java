package kasali.htc.ph.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity implements Camera.PictureCallback, SurfaceHolder.Callback, View.OnClickListener {
    private SurfaceView sv_camera_surface;
    private SurfaceHolder surfaceHolder;
    private Camera camera;
    private boolean isCameraOn = false;
    private FloatingActionButton fab;
    private FloatingActionButton upload;
    private FloatingActionButton videos;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sv_camera_surface = (SurfaceView) findViewById(R.id.sv_camera_surface);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        surfaceHolder = sv_camera_surface.getHolder();
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.addCallback(this);
        sv_camera_surface.setFocusable(true);
        sv_camera_surface.setFocusableInTouchMode(true);
        sv_camera_surface.setOnClickListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        upload = (FloatingActionButton) findViewById(R.id.upload);
        videos = (FloatingActionButton) findViewById(R.id.videos);
        fab.setOnClickListener(this);
        upload.setOnClickListener(this);
        videos.setOnClickListener(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open();

        try{
            camera.setPreviewDisplay(surfaceHolder);
            Camera.Parameters parameters = camera.getParameters();
            if(getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
                parameters.set("orientation", "portrait");
                camera.setDisplayOrientation(90);
            }
        }catch(Exception e){
            camera.release();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();
        camera.release();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sv_camera_surface :
                if(isCameraOn) {
                    camera.takePicture(null, null, this);
                }
                break;
            case R.id.fab:
                sv_camera_surface.setVisibility(View.VISIBLE);
                if (!isCameraOn) {
                    camera.startPreview();
                } else {
                    camera.stopPreview();
                }
                isCameraOn = !isCameraOn;

                break;
            case R.id.videos:

                String url = "http://192.168.42.1/videos/mov_1.avi";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.upload:
                Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "Message is restored!", Snackbar.LENGTH_SHORT);
                snackbar1.show();

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);

                break;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Uri imageFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        try{
            OutputStream imageFile = getContentResolver().openOutputStream(imageFileUri);
            imageFile.write(data);
            imageFile.flush();
            imageFile.close();

        }catch (FileNotFoundException ffe){

        }catch (IOException ioe){

        }
        camera.startPreview();
    }
}
